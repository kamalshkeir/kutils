module github.com/kamalshkeir/kutils

go 1.19

require (
	github.com/kamalshkeir/klog v1.0.0
	golang.org/x/text v0.4.0
)
